import { config } from "dotenv";
import { resolve } from "path";
import { DataSource, DataSourceOptions } from "typeorm";

config({ path: resolve(__dirname, '.env') });

export const dbConfig = {
    type: "mysql",
    host: process.env.DB_HOST || "localhost",
    port: parseInt(process.env.DB_PORT) || 3306,
    username: process.env.DB_USERNAME || "root",
    password: process.env.DB_PASSWORD || "",
    database: process.env.DB_NAME || "nestjs",
    entities: [
        __dirname + '/../**/*.entity{.ts,.js}',
    ],
    migrations: [
        __dirname + '/../**/*.migration{.ts,.js}',
    ],
    synchronize: false,
}

console.log(dbConfig.entities);

const dataSource = new DataSource(dbConfig as DataSourceOptions);

export default dataSource;