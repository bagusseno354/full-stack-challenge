import { MigrationInterface, QueryRunner, Table } from "typeorm";

export default class Initialize1705559064914 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> 
    {
        await queryRunner.createTable(
            new Table({
                name: 'user',                
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,  
                        isGenerated: true,
                        generationStrategy: 'increment'                   
                    },
                    {
                        name: 'username',
                        type: 'varchar',
                        isUnique: true
                    },
                    {
                        name: 'password',
                        type: 'varchar',
                        isUnique: true
                    },
                ],
            }),            
            true,
        );

        await queryRunner.createTable(
            new Table({
                name: 'currency',
                columns: [
                    {
                        name: 'id',
                        type: 'int',
                        isPrimary: true,
                        isGenerated: true,
                        generationStrategy: 'increment'                   
                    },
                    {
                        name: 'base',
                        type: 'varchar',
                        length: '45'
                    },
                    {
                        name: 'counter',
                        type: 'varchar',
                        length: '45'
                    },
                    {
                        name: 'rate',
                        type: 'float',
                    },
                ],
            }),            
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> 
    {
        await queryRunner.dropTable('user');
        await queryRunner.dropTable('currency');
    }

}
