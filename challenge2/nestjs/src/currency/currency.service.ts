import { Inject, Injectable } from '@nestjs/common';
import { CreateCurrencyDto } from './dto/create-currency.dto';
import { UpdateCurrencyDto } from './dto/update-currency.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Currency } from './entities/currency.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CurrencyService 
{
  constructor(
    @InjectRepository(Currency)
    private currency: Repository<Currency>
  ) {}

  async create(createCurrencyDto: CreateCurrencyDto) 
  {
    return await this.currency.insert(createCurrencyDto);
  }

  async findAll(): Promise<Currency[]> 
  {
    return await this.currency.find();
  }

  async findOne(id: number) 
  {
    return await this.currency.findBy({id: id});
  }

  async update(id: number, updateCurrencyDto: UpdateCurrencyDto) 
  {
    console.log(updateCurrencyDto);
    
    const result = await this.currency.update({id: id}, {...updateCurrencyDto, base: updateCurrencyDto.base.toUpperCase(), counter: updateCurrencyDto.counter.toUpperCase()});

    console.log(result);

    return result;  
  }

  async remove(id: number) 
  {
    const data = await this.currency.findBy({id: id});

    if(data.length <= 0)
      return false;      

    return await this.currency.remove(data);
  }
}
