import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Currency 
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    base: string;

    @Column()
    counter: string;

    @Column()
    rate: number;
}
