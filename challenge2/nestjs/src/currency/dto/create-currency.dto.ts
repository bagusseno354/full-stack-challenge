export class CreateCurrencyDto {
    base: string;
    counter: string;
    rate: number;
}
