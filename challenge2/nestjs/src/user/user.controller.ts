import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from './auth/auth.guard';
import { UserService } from './user.service';

@Controller('user')
export class UserController 
{
    constructor(private userService: UserService) {}

    @UseGuards(AuthGuard)
    @Get('me')    
    getCurrentUser(@Request() req)
    {
        return this.userService.findOne(req.user.id);
    }
}
