import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UserService 
{
  constructor(
    @InjectRepository(User)
    private user: Repository<User>
  ) {}

  async findOne(id: number) 
  {
    return await this.user.findOneBy({id: id});
  }
}
