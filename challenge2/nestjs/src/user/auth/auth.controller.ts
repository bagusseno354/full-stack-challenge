import { Body, Controller, Get, Post } from '@nestjs/common';
import { SignInDto } from './dto/sign-in.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController 
{
    constructor(private authService: AuthService) {}

    @Post('signIn')
    signIn(@Body() signInDto: SignInDto)
    {
        return this.authService.signIn(signInDto.username, signInDto.password);
    }
}
