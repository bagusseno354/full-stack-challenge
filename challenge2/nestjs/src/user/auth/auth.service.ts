import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService 
{
    constructor(
        @InjectRepository(User)
        private user: Repository<User>,
        private jwtService: JwtService
    ) {}

    async signIn(username: string, password: string)
    {
        const user = await this.user.findOneBy({username});

        if(!user)
            throw new UnauthorizedException();

        const match = password == user.password;

        if(!match)
            throw new UnauthorizedException();

        const payload = { sub: user.id, username: user.username };

        return {
            access_token: await this.jwtService.signAsync(payload),
        };
    }
}
