import { createSlice } from '@reduxjs/toolkit';
import { user } from '../../../types';

type State = {
  value: user | null
};

export const userSlice = createSlice({
  name: 'user',
  initialState: {value: null} as State,
  reducers: {
    setUser: (state, action) => 
    {
      state.value = action.payload
    },
  },
})

// Action creators are generated for each case reducer function
export const { setUser } = userSlice.actions

export default userSlice.reducer