import { Alert, Button, Card, Form, Input } from "antd";
import { useNavigate } from "react-router-dom";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { useAppDispatch } from "../hooks";
import { setUser } from "../redux/features/user/userSlice";
import config from "../config";
import axios from "axios";
import { useState } from "react";

export default function Login() 
{
  const dispatch = useAppDispatch();
  const [isFailed, setIsFailed] = useState(false);
  const navigate = useNavigate();

  return (
    <div
      className="flex items-center justify-center h-full w-full"
      style={{
        backgroundImage:
          "linear-gradient(to left top, #00255c, #002e70, #003885, #00429a, #004cb0)",
      }}
    >
      <Card className="mt-10">
        <h1>Sign in to CurrencyManager</h1>
        {isFailed && (
          <Alert message="Wrong username or password." type="error" showIcon />
        )}
        <Form
          labelCol={{ span: 8 }}
          className="mt-12"
          onFinish={(data) => {
            const { username, password } = data;

            axios
              .post(`${config.API_BASE_URL}/auth/signIn`, {
                username,
                password,
              })
              .then((res) => {
                const { access_token } = res.data;

                localStorage.setItem("access_token", access_token);

                // get user
                axios
                  .get(`${config.API_BASE_URL}/user/me`, {
                    headers: {
                      Authorization: `Bearer ${access_token}`,
                    },
                  })
                  .then((res) => {
                    dispatch(
                      setUser({
                        username: res.data.username,
                      })
                    );

                    navigate("/");
                  })
                  .catch(() => {
                    return setIsFailed(true);
                  });
              })
              .catch(() => {
                return setIsFailed(true);
              });
          }}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: "Username is required" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Password is required" }]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Button className="w-full" type="primary" htmlType="submit">
              Sign in
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
}
