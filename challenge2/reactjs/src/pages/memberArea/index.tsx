import {
  useNavigate,
  useLocation,
  Outlet,
} from "react-router-dom";
import { Layout, Menu, theme } from "antd";
import { useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks";
import axios from "axios";
import config from "../../config";
import { setUser } from "../../redux/features/user/userSlice";

export default function MemberArea() {
  const { Content, Sider } = Layout;
  const navigate = useNavigate();
  const location = useLocation();
  const dispatch = useAppDispatch();

  const user = useAppSelector((state) => state.user.value);

  // authenticate
  useEffect(() => {
    const access_token = localStorage.getItem("access_token");

    if (!access_token) return navigate("/login");

    if (!user) {
      // get user
      axios
        .get(`${config.API_BASE_URL}/user/me`, {
          headers: {
            Authorization: `Bearer ${access_token}`,
          },
        })
        .then((res) => {
          console.log(res.data.username);
          dispatch(setUser({ username: res.data.username }));
        })
        .catch(() => {
          navigate("/login");
        });
    }
  }, []);

  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  useEffect(() => {
    setSelectedMenuKey(location.pathname);
  }, [location]);

  const [selectedMenuKey, setSelectedMenuKey] = useState("0");
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Layout className="h-full">
      <Sider
        collapsible
        collapsed={collapsed}
        onCollapse={(value) => setCollapsed(value)}
      >
        <div className="text-white text-lg font-bold flex items-center justify-center p-10">
          CurrencyManager
        </div>
        <div className="p-10 text-white">Welcome, {user?.username}</div>
        <Menu
          className="ml-4 flex-1 min-width-0"
          theme="dark"
          selectedKeys={[selectedMenuKey]}
          onClick={(item) => setSelectedMenuKey(item.key)}
          items={[
            {
              key: "/",
              label: "Home",
              onClick: () => navigate("/"),
            },
            {
              key: "/currency",
              label: "Manage currency",
              onClick: () => navigate("/currency"),
            },
            {
              key: "/about",
              label: "About",
              onClick: () => navigate("/about"),
            },
            {
              key: "logOut",
              label: "Log out",
              onClick: () => {
                localStorage.removeItem("access_token");

                navigate("/login");
              },
            },
          ]}
        />
      </Sider>
      <Content style={{ padding: "48px" }}>
        <Layout
          style={{
            padding: "24px 0",
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          <Content className="min-h-[256px] px-10">
            <Outlet />
          </Content>
        </Layout>
      </Content>
    </Layout>
  );
}
