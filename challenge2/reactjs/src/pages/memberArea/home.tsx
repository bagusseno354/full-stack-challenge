export default function Home() {
  return (
    <div>
      <h1>Welcome!</h1>
      Welcome to CurrencyManager by Bagus Seno Pamungkas. You can start managing
      the currencies through the 'Manage Currency' menu on the left. Enjoy!
    </div>
  );
}
