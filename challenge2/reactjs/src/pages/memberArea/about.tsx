export default function About() {
  return (
    <div>
      <h1>About this app</h1>
      <div className="mt-">
        CurrencyManager is a web app that stores exchange rate between
        currencies by Bagus Seno for TRAXX Payments. This web app was built
        using ReactJS (through create-react-app), react-redux, react-router-dom,
        TailwindCSS, and ANTD components. Meanwhile, on the back-end, it uses
        NestJS, TypeORM, and JWT.
      </div>
    </div>
  );
}
