import React, { useEffect, useState } from "react";
import {
  Form,
  Input,
  InputNumber,
  Popconfirm,
  notification,
  Table,
  Typography,
  FloatButton,
  Modal,
  Button,
} from "antd";
import { PlusOutlined } from "@ant-design/icons";
import axios from "axios";
import config from "../../config";
import { useNavigate } from "react-router-dom";

interface Item {
  key: number;
  id: number;
  base: string;
  counter: string;
  rate: number;
}

interface EditableCellProps extends React.HTMLAttributes<HTMLElement> 
{
  editing: boolean;
  dataIndex: string;
  title: any;
  inputType: "number" | "text";
  record: Item;
  index: number;
  children: React.ReactNode;
}

const EditableCell: React.FC<EditableCellProps> = ({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode =
    inputType === "number" ? <InputNumber className="w-full" /> : <Input className="uppercase" />;

  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          style={{ margin: 0 }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode}
        </Form.Item>
      ) : (
        children
      )}
    </td>
  );
};

export default function CurrencyManager() 
{
  const [form] = Form.useForm();
  const [insertForm] = Form.useForm();
  const [data, setData] = useState<Item[]>([]);
  const [editingKey, setEditingKey] = useState(-1);
  const [isAddDataModalOpen, setIsAddDataModalOpen] = useState(false);
  const [api, contextHolder] = notification.useNotification();

  const navigate = useNavigate();

  useEffect(() => 
  {
    const access_token = localStorage.getItem("access_token");

    axios
      .get(`${config.API_BASE_URL}/currency`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then((res) => {
        if (res.status == 200) {
          res.data = res.data.map((item: Item, index: number) => ({
            ...item,
            key: index,
          }));
          setData(res.data);
        }
      })
      .catch((err) => {
        console.error(err);

        api.error({ message: "Failed inserting data" });

        if (err.response.status == 401) navigate("/login");
      });
  }, []);

  const isEditing = (record: Item) => record.id === editingKey;

  const edit = (record: Partial<Item> & { id: number }) => 
  {
    form.setFieldsValue({ name: "", age: "", address: "", ...record });
    setEditingKey(record.id);
  };

  const cancel = () => 
  {
    setEditingKey(-1);
  };

  const save = async (id: number) => 
  {
    const access_token = localStorage.getItem("access_token");

    let row: Item | null = null;

    try {
      row = (await form.validateFields()) as Item;
    } catch (err) {
      console.error(err);
    }

    if (!row) return;

    row = {
      ...row,
      base: row.base.toUpperCase(),
      counter: row.counter.toUpperCase(),
    };

    // update database first
    axios
      .patch(`${config.API_BASE_URL}/currency/${id}`, row, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then((res) => {
        if (!row) return;
        if (res.data.affected == 0)
          return api.error({ message: "Failed updating data" });

        api.success({ message: "Succeed updating data" });

        const newData: Item[] = [...data];
        const index = newData.findIndex((item) => id === item.id);

        if (index > -1) {
          const item = newData[index];

          newData.splice(index, 1, {
            ...item,
            ...row,
          });

          setData(newData);
          setEditingKey(-1);
        } else {
          newData.push(row);
          setData(newData);
          setEditingKey(-1);
        }
      })
      .catch((err) => {
        console.error(err);

        if (err.response.status == 401) navigate("/login");
      });
  };

  const remove = async (id: number) => 
  {
    const access_token = localStorage.getItem("access_token");

    // update database first
    axios
      .delete(`${config.API_BASE_URL}/currency/${id}`, {
        headers: {
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then((res) => {
        if (res.data.length == 0)
          return api.error({ message: "Failed deleting data" });

        api.success({ message: "Succeed deleting data" });

        const newData: Item[] = [...data];
        const index = newData.findIndex((item) => id === item.id);

        if (index > -1) {
          newData.splice(index, 1);

          setData(newData);
          setEditingKey(-1);
        }
      })
      .catch((err) => {
        console.error(err);

        api.error({ message: "Failed deleting data" });

        if (err.response.status == 401) navigate("/login");
      });
  };

  const columns = [
    {
      title: "Base",
      dataIndex: "base",
      editable: true,
    },
    {
      title: "Counter",
      dataIndex: "counter",
      editable: true,
    },
    {
      title: "Rate",
      dataIndex: "rate",
      editable: true,
    },
    {
      title: "Action",
      width: "10%",
      dataIndex: "action",
      render: (_: any, record: Item) => {
        const editable = isEditing(record);
        return editable ? (
          <span>
            <Typography.Link
              onClick={() => save(record.id)}
              style={{ marginRight: 8 }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <span className="flex gap-4">
            <Typography.Link
              disabled={editingKey !== -1}
              onClick={() => edit(record)}
            >
              Edit
            </Typography.Link>
            {editingKey == -1 && (
              <Popconfirm
                okType="danger"
                okText="Delete"
                title="Are you sure to delete?"
                onConfirm={() => remove(record.id)}
              >
                <a>Delete</a>
              </Popconfirm>
            )}
          </span>
        );
      },
    },
  ];

  const mergedColumns = columns.map((col) => 
  {
    if (!col.editable) 
    {
      return col;
    }
    return {
      ...col,
      onCell: (record: Item) => ({
        record,
        inputType: col.dataIndex === "rate" ? "number" : "text",
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });

  return (
    <div>
      {contextHolder}
      <h1>Manage currency</h1>
      <FloatButton
        type="primary"
        icon={<PlusOutlined />}
        onClick={() => setIsAddDataModalOpen(true)}
      />
      <Modal
        title="Add currency"
        open={isAddDataModalOpen}
        footer={null}
        onCancel={() => setIsAddDataModalOpen(false)}
      >
        <Form
          form={insertForm}
          onFinish={(data) => {
            data = {
              ...data,
              base: data.base.toUpperCase(),
              counter: data.counter.toUpperCase(),
            };
            const access_token = localStorage.getItem("access_token");

            if (!access_token) {
              navigate("/login");
            }

            axios
              .post(`${config.API_BASE_URL}/currency`, data, {
                headers: {
                  Authorization: `Bearer ${access_token}`,
                },
              })
              .then((res) => {
                if (res.status != 201) return;

                data = { key: res.data.raw.insertId, id: res.data.raw.insertId, ...data };

                setData((oldData) => [...oldData, data]);

                api.success({
                  message: "Succeed inserting data",
                  placement: "topRight",
                });
                insertForm.resetFields();
                setIsAddDataModalOpen(false);
              })
              .catch((err) => {
                console.error(err);

                if (err.response.status == 401) navigate("/login");
              });
          }}
          className="mt-8"
          labelAlign="left"
          labelCol={{ span: 4 }}
        >
          <Form.Item name="base" label="Base">
            <Input className="uppercase" />
          </Form.Item>
          <Form.Item name="counter" label="Counter">
            <Input className="uppercase" />
          </Form.Item>
          <Form.Item name="rate" label="Rate">
            <InputNumber className="w-full" />
          </Form.Item>
          <Form.Item>
            <Button className="w-full" type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <Form form={form} component={false}>
        <Table
          components={{
            body: {
              cell: EditableCell,
            },
          }}
          bordered
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
          pagination={{
            onChange: cancel,
          }}
        />
      </Form>
    </div>
  );
}
