import './App.css';
import { Route, Routes } from 'react-router-dom';
import Login from './pages/login';
import store from './redux/store';
import { Provider } from 'react-redux';
import CurrencyManager from './pages/memberArea/manager';
import Home from './pages/memberArea/home';
import About from './pages/memberArea/about';
import MemberArea from './pages/memberArea';

function App() 
{
  return (
    <Provider store={store}>
      <Routes>
        <Route path='/login' element={<Login />} />
        <Route path='/' element={<MemberArea />} >
          <Route index={true} element={<Home />} />
          <Route path='/about' element={<About />} />                
          <Route path='/currency' element={<CurrencyManager />} />   
        </Route>
      </Routes>
    </Provider>
  );
}

export default App;
