<?php

namespace App\Http\Controllers;

class InputController extends Controller
{
    public function input()
    {
        return view('input');
    }

    public function view()
    {
        return view('view');
    }
}