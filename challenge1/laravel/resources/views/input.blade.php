<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @vite('resources/css/app.scss')
        <style>
            html, body {
                min-height: 100%
            }
            body {            
                background: linear-gradient( 135deg, #F761A1 10%, #8C1BAB 100%)
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <title>Input Form</title>
    </head>
    <body class="px-2">
        <div class="w-full h-full flex justify-center">
            <div class="w-full max-w-[512px] mt-8 shadow p-8 bg-white rounded">
                <h1 class="text-xl font-semibold">
                    Input Form
                </h1>
                <div class="mt-2 text-black/50">
                    Welcome to the input page! This page was built by Bagus Seno as a completion of a challenge by TRAXX company.
                    The purpose of this form is to show the inputted data to another view. Enjoy!
                </div>
                <form action="{{ url('/') }}/view" method="POST" class="mt-8">
                    @csrf
                    <div class="flex gap-4">
                        <div class="form__group field flex-1">
                            <label for="name" class="form__label">First name</label>
                            <input type="input" class="form__field" name="firstName" required required data-tippy-content="UX info: The first name has a validator that only allows alphanumeric. Also, for all fields, the values are set to bold to distinguish them from the labels, hence they are more easily noticed as value by users, lowering effort of users = better UX." />
                            <div class="text-xs mt-1 text-[red] hidden error-alert">
                                Required
                            </div>
                        </div>
                        <div class="form__group field flex-1">
                            <label for="name" class="form__label">Last name</label>
                            <input type="input" class="form__field" name="lastName" required data-tippy-content="UX info: The last name has a validator that only allows alphanumeric." />
                            <div class="text-xs mt-1 text-[red] hidden error-alert">
                                Required
                            </div>
                        </div>
                    </div>
                    <div class="form__group field flex-1">
                        <label for="name" id="contact-label" class="form__label">Contact number</label>
                        <input type="hidden" name="countryCode">
                        <input type="text" class="form__field" name="contactNumber" id="contact-number" required data-tippy-content="UX info: The contact number field has a validator that sets the minimum length of seven numbers including the country code. Try changing the country code, the minimum length will be adjusted to the new country code." />
                        <div class="text-xs mt-1 text-[red] hidden error-alert">
                            Required
                        </div>
                    </div>                    
                    <div class="form__group field flex-1">
                        <label for="name" class="form__label">Email</label>
                        <input type="email" class="form__field" placeholder="Email" name="email" required data-tippy-content="UX info: This email field has a validator with a syntax based on RFC 1035 that is recommended by RFC 5322 with some adjustments. The rule includes that dots are allowed, but not at the end and not consecutive. Also, some other symbols are allowed." />
                        <div class="text-xs mt-1 text-[red] hidden error-alert">
                            Invalid email
                        </div>
                    </div>
                    <div class="form__group field flex-1">
                        <label for="name" class="form__label">Website/LinkedIn</label>
                        <input type="text" class="form__field" name="url" required data-tippy-content="UX info: The website/LinkedIn field has a URL validator that allows both either using https:// or not to increase the user's flexibility. Sometimes a user types the domain of their website only (without https://) since it's easier and faster. Sometimes a user will copy the URL directly from the browser that contains the https://. In order to standardize the format in database, it's our task at the backend." />
                        <div class="text-xs mt-1 text-[red] hidden error-alert">
                            Invalid URL
                        </div>
                    </div>
                    <div class="form__group field w-full">
                        <input type="submit" class="button-5 w-full" required data-tippy-placement="bottom" data-tippy-content="UX info: The color is set to blue since it's a contrast color to the background, hence makes user easily notice it as a button. The color is also bright enough to increase the clickability feel." />
                    </div>
                </form>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
    <script src="https://unpkg.com/tippy.js@5"></script>
    <script>
        const contactNumber = document.getElementById('contact-number');

        $('#contact-number').intlTelInput({
            separateDialCode: true,
        });

        $('[name="countryCode"]').val($('#contact-number').intlTelInput('getSelectedCountryData').dialCode)

        $('#contact-number').on('countrychange', () => 
        {
            $('[name="countryCode"]').val($('#contact-number').intlTelInput('getSelectedCountryData').dialCode)
        });

        var constraints = {
            firstName: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[A-Za-z0-9 ]*$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format('^Symbols are not allowed');
                    }
                }
            },
            lastName: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[A-Za-z0-9 ]*$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format('^Symbols are not allowed');
                    }
                }
            },
            contactNumber: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[0-9]*$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format('^Invalid phone number');
                    }
                },
                length: function(value, attributes, attributeName, options, constraints) 
                {
                    const dialCode = $('#contact-number').intlTelInput('getSelectedCountryData').dialCode;

                    return {minimum: 7 - dialCode.length}
                }
            },
            email: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format("^Invalid email", {
                            email: value
                        });
                    }
                },                
            },
            url: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9!@#$%^&*(-_+{}\]\[":;'\/><.,)#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format("^Invalid URL", {
                            url: value
                        });
                    }
                },                
            },
        };

        function validateByKey(key)
        {
            const constraint = constraints[key];
            const value = $(`[name="${key}"]`).val();
            const errors = validate({[key]: value}, {[key]: constraint});

            if(errors)
            {
                const errorText = errors[key][0];
                
                $(`[name="${key}"]`).parent().find('.error-alert').html(errorText).show().length > 0 || $(`[name="${key}"]`).parent().next('.error-alert').html(errorText).show();

                return false;
            }
            else
            {
                $(`[name="${key}"]`).parent().find('.error-alert').hide().length > 0 || $(`[name="${key}"]`).parent().next('.error-alert').hide();

                return true;
            }
        }

        // custom error appearance: show error near the field
        Object.keys(constraints).forEach(key =>
        {
            $(`[name="${key}"]`).keyup(e =>
            {
                // prevent shift and tab trigger the error checking. If this is not
                // implemented, when the user tabs to the next field, if the next field is required
                // then it will immediately show error: field is required.
                if(e.keyCode == 9 || e.keyCode == 16)
                    return;

                validateByKey(key);
            });
        });

        $('[type="submit"').click(e =>
        {            
            let isAnyError = false;

            Object.keys(constraints).forEach(key =>
            {
                if(!validateByKey(key))
                    isAnyError = true;
            });

            if(isAnyError)
                e.preventDefault();
        });

    </script>
    <script>
        tippy('[data-tippy-content');
    </script>
</html>