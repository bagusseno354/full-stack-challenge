<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @vite('resources/css/app.scss')
        <style>
            html, body {
                min-height: 100%
            }
            body {            
                background: linear-gradient( 135deg, #F761A1 10%, #8C1BAB 100%)
            }
        </style>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/css/intlTelInput.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
        <title>View Page</title>
    </head>
    <body class="px-2">
        <div class="w-full h-full flex justify-center">
            <div class="w-full max-w-[512px] mt-8 shadow p-8 bg-white rounded">
                <h1 class="text-xl font-semibold flex gap-4">
                    <a class="!text-black !no-underline" href="{{ url('') }}">←</a>
                    <span>
                        View Page
                    </span>
                </h1>
                <div class="mt-2 text-black/50">
                    Welcome to the view page! This page was built by Bagus Seno as a completion of a challenge by TRAXX company.
                    The purpose of this page is to view the inputted data from <a href="{{ url('') }}">this form page</a>. Enjoy!
                </div>
                <div class="mt-8 break-words data-list">  
                    <div>
                        <span>First name</span> 
                        <span>{{ isset($_POST['firstName']) ? $_POST['firstName'] : 'data is empty :(' }}</span>                    
                    </div>                 
                    <div>
                        <span>Last name</span> 
                        <span>{{ isset($_POST['lastName']) ? $_POST['lastName'] : 'data is empty :(' }}</span> 
                    </div>
                    <div>
                        <span>Contact</span> 
                        <span>{{ isset($_POST['contactNumber']) ? '+' . $_POST['countryCode'] . ' ' . $_POST['contactNumber'] : 'data is empty :(' }}</span> 
                    </div>                 
                    <div>
                        <span>Email</span> 
                        <span>{{ isset($_POST['email']) ? $_POST['email'] : 'data is empty :(' }}</span> 
                    </div>
                    <div>
                        <span>Website or LinkedIn</span> 
                        <span>{{ isset($_POST['url']) ? $_POST['url'] : 'data is empty :(' }}</span> 
                    </div>                 
                </div>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.13/js/intlTelInput-jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.13.1/validate.min.js"></script>
    <script>
        const contactNumber = document.getElementById('contact-number');

        $('#contact-number').intlTelInput({
            separateDialCode: true,
        });

        $('#contact-number').on('countrychange', () => 
        {
            $('[name="country-code"]').val($('#contact-number').intlTelInput('getSelectedCountryData').dialCode)
        });

        var constraints = {
            firstName: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[A-Za-z0-9 ]*$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format('^Symbols are not allowed');
                    }
                }
            },
            lastName: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[A-Za-z0-9 ]*$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format('^Symbols are not allowed');
                    }
                }
            },
            contactNumber: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[0-9]{4,15}$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format('^Invalid phone number');
                    }
                }
            },
            email: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,63}$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format("^Invalid email", {
                            email: value
                        });
                    }
                },                
            },
            url: {
                presence: {
                    allowEmpty: false,
                    message: 'is required'
                },
                format: {
                    pattern: /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9!@#$%^&*(-_+{}\]\[":;'\/><.,)#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
                    message: function(value, attribute, validatorOptions, attributes, globalOptions) {
                        return validate.format("^Invalid URL", {
                            url: value
                        });
                    }
                },                
            },
        };

        function validateByKey(key)
        {
            const constraint = constraints[key];
            const value = $(`[name="${key}"]`).val();
            const errors = validate({[key]: value}, {[key]: constraint});

            if(errors)
            {
                const errorText = errors[key][0];
                
                $(`[name="${key}"]`).parent().find('.error-alert').html(errorText).show().length > 0 || $(`[name="${key}"]`).parent().next('.error-alert').html(errorText).show();

                return false;
            }
            else
            {
                $(`[name="${key}"]`).parent().find('.error-alert').hide().length > 0 || $(`[name="${key}"]`).parent().next('.error-alert').hide();

                return true;
            }
        }

        // custom error appearance: show error near the field
        Object.keys(constraints).forEach(key =>
        {
            $(`[name="${key}"]`).keyup(e =>
            {
                // prevent shift and tab trigger the error checking. If this is not
                // implemented, when the user tabs to the next field, if the next field is required
                // then it will immediately show error: field is required.
                if(e.keyCode == 9 || e.keyCode == 16)
                    return;

                validateByKey(key);
            });
        });

        $('[type="submit"').click(e =>
        {            
            let isAnyError = false;

            Object.keys(constraints).forEach(key =>
            {
                if(!validateByKey(key))
                    isAnyError = true;
            });

            if(isAnyError)
                e.preventDefault();
        });

    </script>
</html>